import WellFormedFormulas
import Sat
import Parser

solve1 :: String -> [Valuation]
solve1 = Sat.sat1 . Parser.wffFromString

solve2 :: String -> [Valuation]
solve2 = Sat.sat2 . Parser.wffFromString

solve3 :: String -> IO ()
solve3 = Sat.sat3 . Parser.wffFromString
