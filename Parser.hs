module Parser (wffFromString) where
    import qualified Data.Map as Map
    import WellFormedFormulas

    wffFromString :: String -> WFF
    wffFromString str =
        if ((length tmp) == 1)
            then WFF (head tmp)
            else case tmp!!1 of
                "and" -> And (wffFromString (head tmp)) (wffFromString (last tmp))
                "or" -> Or (wffFromString (head tmp)) (wffFromString (last tmp))
                "not" -> Not (wffFromString (head tmp))
                "->" -> Implies (wffFromString (head tmp)) (wffFromString (last tmp))
                "iff" -> Iff (wffFromString (head tmp)) (wffFromString (last tmp))
        where tmp = search1 (words str)

    search1 :: [String] -> [String]
    search1 ("(":strs) = first : (head rest) : search2 (tail rest)
        where (first,rest) = untilBalanced strs 1 []
    search1 ("not":strs) = fst (untilBalanced strs 0 []) : ["not"]
    search1 (v:[]) = [v]
    search1 (v:strs) = v : (head strs) : search2 (tail strs)

    search2 :: [String] -> [String]
    search2 ("(":strs) = [fst $ untilBalanced strs 1 []]
    search2 (v:strs) = [v]

    untilBalanced :: [String] -> Int -> String -> (String,[String])
    untilBalanced ("(":strs) count ac = untilBalanced strs (count + 1) (ac ++ " (")
    untilBalanced (")":strs) count ac =
        if (count - 1 == 0)
            then (ac,strs)
            else untilBalanced strs (count - 1) (ac ++ " )")
    untilBalanced (s:strs) count ac = untilBalanced strs count (ac ++ " " ++ s)
    -- untilBalanced [] count ac = (ac ++ " (", [])
