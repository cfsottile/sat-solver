
module WellFormedFormulas
    ( PropVar(..)
    , WFF(..)
    , Valuation(..)
    , foldWFF
    , check
    , propVars
    ) where

import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.Maybe as Maybe

type PropVar = String
data WFF = WFF PropVar | And WFF WFF | Or WFF WFF | Not WFF | Implies WFF WFF | Iff WFF WFF deriving Show
type Valuation = Map.Map PropVar Bool

foldWFF :: (PropVar -> b) -> (b -> b -> b) -> (b -> b -> b) -> (b -> b) -> (b -> b -> b) -> (b -> b -> b) -> WFF -> b
-- recursion over WFF structure
foldWFF f1 f2 f3 f4 f5 f6 (WFF pv) = f1 pv
foldWFF f1 f2 f3 f4 f5 f6 (And wff1 wff2) = f2 (foldWFF f1 f2 f3 f4 f5 f6 wff1) (foldWFF f1 f2 f3 f4 f5 f6 wff2)
foldWFF f1 f2 f3 f4 f5 f6 (Or wff1 wff2) = f3 (foldWFF f1 f2 f3 f4 f5 f6 wff1) (foldWFF f1 f2 f3 f4 f5 f6 wff2)
foldWFF f1 f2 f3 f4 f5 f6 (Not wff) = f4 (foldWFF f1 f2 f3 f4 f5 f6 wff)
foldWFF f1 f2 f3 f4 f5 f6 (Implies wff1 wff2) = f5 (foldWFF f1 f2 f3 f4 f5 f6 wff1) (foldWFF f1 f2 f3 f4 f5 f6 wff2)
foldWFF f1 f2 f3 f4 f5 f6 (Iff wff1 wff2) = f6 (foldWFF f1 f2 f3 f4 f5 f6 wff1) (foldWFF f1 f2 f3 f4 f5 f6 wff2)

opAnd :: Bool -> Bool -> Bool
opAnd False _ = False
opAnd True b = b

opOr :: Bool -> Bool -> Bool
opOr True _ = True
opOr False b = b

opNot :: Bool -> Bool
opNot True = False
opNot False = True

opImplies :: Bool -> Bool -> Bool
opImplies True False = False
opImplies _ _ = True

opIff :: Bool -> Bool -> Bool
opIff b1 b2 = b1 == b2

check :: WFF -> Valuation -> Bool
check wff val = foldWFF (\pv -> Maybe.fromJust (Map.lookup pv val)) opAnd opOr opNot opImplies opIff wff

propVars :: WFF -> [PropVar]
propVars = List.nub . (foldWFF (:[]) (++) (++) id (++) (++))
