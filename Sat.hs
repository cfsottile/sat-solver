module Sat
    ( sat1
    , sat2
    , sat3
    , sat4
    ) where

import qualified Data.Map as Map
import qualified Data.Maybe as Maybe
import qualified Data.List as List
import WellFormedFormulas

import Control.Concurrent (forkIO)
import Control.Concurrent.Chan
import Control.Concurrent.QSemN
import Control.Concurrent.QSem

-- some algorithm

cartProd :: [[(PropVar,Bool)]] -> [[(PropVar,Bool)]] -> [[(PropVar,Bool)]]
cartProd xss yss = List.nub [ concatl xs ys | xs <- xss, ys <- yss ]

-- concatL porque, ante la repetición de una asignación de valor para una variable,
-- descarta siempre la repetición ocurrida en la segunda lista, manteniendo la ocurrencia
-- en la primer lista (la lista de la izquierda) y asegurando así consistencia.
concatl :: [(PropVar,Bool)] -> [(PropVar,Bool)] -> [(PropVar,Bool)]
concatl xs [] = xs
concatl xs ((pv,v):ys) =
    if (Maybe.isNothing (lookup pv xs))
        then concatl ((pv,v):xs) ys
        else concatl xs ys

valuations :: WFF -> [Valuation]
valuations wff = map Map.fromList valuationsLists
    where
        valuationsLists =
            foldWFF
                (\pv -> [[(pv,True)],[(pv,False)]]) cartProd cartProd id cartProd cartProd wff

sat1 :: WFF -> [Valuation]
sat1 wff = filter (check wff) (valuations wff)

-- recursion over

satValuations :: (Valuation -> Bool) -> Valuation -> [PropVar] -> [Valuation]
-- recursion over [PropVar]
satValuations s val [] = if s val then [val] else []
satValuations s val (v:vars) =
    satValuations s (Map.insert v True val) vars ++ satValuations s (Map.insert v False val) vars

sat2 :: WFF -> [Valuation]
sat2 wff = satValuations (check wff) Map.empty (propVars wff)

-- concurrent1

-- satValuations' :: (Valuation -> Bool) -> Valuation -> [PropVar] -> [Valuation]
-- recursion over [PropVar]
satValuations' s val [] chan = do
    if s val then putStrLn (show val)
             else return ()
    writeChan chan ""
satValuations' s val (v:vars) chan = do
    forkIO $ do
        satValuations' s (Map.insert v False val) vars chan
    satValuations' s (Map.insert v True val) vars chan
    return ()

-- sat2 :: WFF -> [Valuation]
sat3 wff = do
    chan <- newChan
    satValuations' (check wff) Map.empty (propVars wff) chan
    waitMessages (2^(length (propVars wff))) chan
    -- waitMessages 2 chan
    return ()

waitMessages 0 chan = return ()
waitMessages n chan = do
    readChan chan
    waitMessages (n - 1) chan

-- concurrent2

satValuations2 s val [] sem =
    if s val
        then do
            -- putStrLn (show val)
            signalQSemN sem 1
        else do
            signalQSemN sem 1

satValuations2 s val (v:vars) sem = do
    forkIO $ do
        satValuations2 s (Map.insert v False val) vars sem
    satValuations2 s (Map.insert v True val) vars sem
    return ()

sat4 wff = do
    let pvs = propVars wff
        threadsAmount = 2^(length pvs)
    sem <- newQSemN threadsAmount
    satValuations2 (check wff) Map.empty (pvs) sem
    -- waitSem threadsAmount sem
    waitQSemN sem threadsAmount

-- waitSem 0 sem = return ()
-- waitSem n sem = do
--     waitQSem sem
--     waitSem (n - 1) sem
